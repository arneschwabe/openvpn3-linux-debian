#!/usr/bin/python3

import sys
import os.path

dists = {
    "ubuntu16":
    {
        'pkglist': 'build-essential git pkg-config autoconf autoconf-archive libglib2.0-dev libjsoncpp-dev uuid-dev libmbedtls-dev liblz4-dev libcap-ng-dev git-buildpackage debhelper dh-python dh-autoreconf python3-docutils selinux-policy-dev libxml2-utils',
        'baseimage': 'ubuntu:18.04',
        'template': 'Dockerfile.deb.tmpl'
    },
    "ubuntu18":
    {
        'pkglist': 'build-essential git pkg-config autoconf autoconf-archive libglib2.0-dev libjsoncpp-dev uuid-dev libmbedtls-dev liblz4-dev libcap-ng-dev git-buildpackage debhelper dh-python dh-autoreconf python3-docutils selinux-policy-dev libxml2-utils',
        'baseimage': 'ubuntu:18.04',
        'template': 'Dockerfile.deb.tmpl'
    },
    "debian9":
    {
        'pkglist': 'build-essential git pkg-config autoconf autoconf-archive libglib2.0-dev libjsoncpp-dev uuid-dev libmbedtls-dev liblz4-dev libcap-ng-dev git-buildpackage debhelper dh-python dh-autoreconf python3-docutils selinux-policy-dev libxml2-utils',
        'baseimage': 'debian:stretch',
        'template': 'Dockerfile.deb.tmpl'
    },
}

def main():
    dist = sys.argv[1]
    output = sys.argv[2]

    scriptdir = os.path.dirname(os.path.realpath(__file__))

    gen_template(dist, scriptdir, output)


def gen_template(dist, tmpldir, output):
    vars = dists[dist]
    vars['dist'] = dist

    tmplfile = os.path.join(tmpldir, vars['template'])
    template = open(tmplfile).read()

    open(output, "w").write(template % vars)
    
    
if __name__ == '__main__':
    main()
