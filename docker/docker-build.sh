#!/bin/bash

set -e

# get theroot directory
ROOTDIR=$(realpath $(dirname "${BASH_SOURCE[0]}")/..)

function run() {
    PLAT=$1

    dockerfile=$(mktemp)
    python3 ${ROOTDIR}/docker/buildtempl.py ${PLAT} ${dockerfile}

    docker build $ROOTDIR -f ${dockerfile} --build-arg cores=$(nproc) -t o3debbuild/${PLAT} --target o3debprep-${PLAT} -t o3debprep/${PLAT}
    docker build $ROOTDIR -f ${dockerfile}  -t o3debbuild/${PLAT} --target o3debbuild-${PLAT} -t o3debbuild/${PLAT}

    echo "Running build for ${PLAT}"
    if  docker run --name temp-o3debbuild-${PLAT} o3debbuild/${PLAT}; then
        echo build succeeded
        
        docker cp temp-o3debbuild-${PLAT}:/root/dest-${PLAT} .
        EXIT=0
    else
        EXIT=1
        echo build failed        
    fi

    # Remove
    if [ -n "${NODELETE}" ]; then
        echo $dockerfile
        exit 3
    fi
    
    echo "Removing temporary docker images"
    docker rm temp-o3debbuild-${PLAT} || true
    docker image rm o3debbuild/${PLAT} || true
    rm ${dockerfile}
    if [ ${EXIT} = 1 ]; then
        exit 1
    fi
}


if [ -z "$@" ]; then
    PLATFORMS='ubuntu16 ubuntu18 debian9'
else
    PLATFORMS="$@"
fi

for PLAT in $PLATFORMS
do
	run $PLAT
done

echo finished files:
for PLAT in $PLATFORMS; do
    ls dest-${PLAT}/*
done


